package com.example.swedbank.journal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class EventRepositoryImplTest {

    @Autowired
    private EventRepository repository;

    @Test
    void canStartAndFinishEvent() {
        final int userId = 1234;
        final int siteId = 5678;

        repository.startEvent(userId, siteId, EventType.PLAYING);

        Event startedEvent = repository.getLastEventsByUserId(userId).get(0);
        assertThat(startedEvent.getUserId()).isEqualTo(userId);
        assertThat(startedEvent.getEventType()).isEqualTo(EventType.PLAYING);
        assertThat(startedEvent.getStartedAt()).isNotNull();
        assertThat(startedEvent.getFinishedAt()).isNull();

        repository.finishEvent(userId);
        Event finishedEvents = repository.getLastEventsByUserId(userId).get(0);
        assertThat(finishedEvents.getFinishedAt()).isNotNull();
    }

    @Test
    void shouldReturnEventsInReversedOrder() {
        final int userId = 5678;
        final int siteId = 1234;

        repository.startEvent(userId, siteId, EventType.PLAYING);
        repository.startEvent(userId, siteId, EventType.IN_QUEUE);

        List<Event> events = repository.getAll().collect(Collectors.toList());
        assertThat(events.get(0).getEventType()).isEqualTo(EventType.IN_QUEUE);
        assertThat(events.get(1).getEventType()).isEqualTo(EventType.PLAYING);
    }
}