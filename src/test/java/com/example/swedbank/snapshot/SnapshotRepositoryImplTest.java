package com.example.swedbank.snapshot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

import static com.example.swedbank.snapshot.Snapshot.builder;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class SnapshotRepositoryImplTest {

    @Autowired
    private SnapshotRepository repository;

    @Test
    void shouldAddAndGetSnapshot() {
        LocalDateTime dateTime = LocalDateTime.now();

        Snapshot snapshot = builder().siteId(1).utilization(100).dateTime(dateTime).build();
        repository.addSnapshot(snapshot);

        Map<LocalDateTime, Integer> utilization = repository.getUtilizationBySiteId(1);
        assertThat(utilization.get(dateTime)).isEqualTo(100);
    }

    @Test
    void shouldBeLatestAndLimitedSnapshot() {
        LocalDateTime time1 = LocalDateTime.now();
        LocalDateTime time2 = LocalDateTime.now().minusHours(1);
        LocalDateTime time3 = LocalDateTime.now().plusHours(1);

        Snapshot snapshot1 = builder().siteId(1).utilization(10).dateTime(time1).build();
        Snapshot snapshot2 = builder().siteId(1).utilization(20).dateTime(time2).build();
        Snapshot snapshot3 = builder().siteId(1).utilization(30).dateTime(time3).build();
        repository.addSnapshot(snapshot1);
        repository.addSnapshot(snapshot2);
        repository.addSnapshot(snapshot3);

        Map<LocalDateTime, Integer> map = repository.getUtilizationBySiteId(1);
        assertThat(map.size()).isEqualTo(2);
        assertThat(map.keySet()).isEqualTo(Set.of(time1, time3));
    }

}