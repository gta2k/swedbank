package com.example.swedbank.snapshot;

import com.example.swedbank.playsite.PlaySite;
import com.example.swedbank.playsite.types.Carousel;
import com.example.swedbank.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class SnapshotServiceTest {

    @Autowired
    SnapshotService snapshotService;

    @Test
    void shouldDropSecondsAndNanos() {
        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setName("Test site");
        playSite.setMaxCapacity(1);
        playSite.setUsersOnSite(List.of(new User()));

        LocalDateTime snapshotTime = LocalDateTime.now();
        Snapshot snapshot = snapshotService.takeSnapshot(playSite);

        assertThat(snapshot.getSiteId()).isEqualTo(1);
        assertThat(snapshot.getUtilization()).isEqualTo(100);
        assertThat(snapshot.getDateTime()).isEqualTo(snapshotTime.withSecond(0).withNano(0));    }

}