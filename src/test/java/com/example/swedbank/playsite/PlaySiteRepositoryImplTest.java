package com.example.swedbank.playsite;

import com.example.swedbank.exception.PlaySiteNotFound;
import com.example.swedbank.playsite.types.Carousel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@SpringBootTest
class PlaySiteRepositoryImplTest {

    @Autowired
    private PlaySiteRepository repository;

    @Test
    void shouldFindJustSavedUser() {
        PlaySite playSite = repository.add(new Carousel());
        assertThat(repository.getById(playSite.getId())).isEqualTo(playSite);
    }

    @Test
    void shouldThrowException() {
        assertThatThrownBy(() -> repository.getById(0))
                .isInstanceOf(PlaySiteNotFound.class);
    }

    @Test
    void shouldHaveIncreasedPlaySiteId() {
        int lastId = repository.getAll().size();

        PlaySite newPlaySite = repository.add(new Carousel());

        assertThat(newPlaySite.getId()).isEqualTo(lastId + 1);
    }
}