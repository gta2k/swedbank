package com.example.swedbank.playsite;

import com.example.swedbank.playsite.types.Carousel;
import com.example.swedbank.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class PlaySiteServiceTest {

    @Autowired private PlaySiteService service;
    @MockBean private PlaySiteRepository repository;

    @Test
    void shouldAddUserToSite() {
        User user = new User();
        PlaySite site = new Carousel();
        List<User> users = service.addUserToSite(user, site);
        assertThat(site.getUsersOnSite()).isEqualTo(users);
        assertThat(site.getUsersInQueue()).isEqualTo(List.of());
    }

    @Test
    void shouldAddUserToQueue() {
        User user = new User();
        PlaySite site = new Carousel();
        List<User> users = service.addUserToQueue(user, site);
        assertThat(site.getUsersOnSite()).isEqualTo(List.of());
        assertThat(site.getUsersInQueue()).isEqualTo(users);
    }

    @Test
    void shouldRemoveUserFromSiteAndQueue() {
        User user1 = new User();
        User user2 = new User();
        List<User> onSite = new ArrayList<>();
        List<User> inQueue = new ArrayList<>();
        onSite.add(user1);
        onSite.add(user2);

        PlaySite site = new Carousel();
        site.setUsersOnSite(onSite);
        site.setUsersInQueue(inQueue);

        service.removeUser(user1, site);
        service.removeUser(user2, site);
        assertThat(site.getUsersOnSite().isEmpty()).isTrue();
        assertThat(site.getUsersInQueue().isEmpty()).isTrue();
    }

    @Test
    void shouldFindUserOnSite() {
        User user = new User();
        PlaySite site = new Carousel();
        site.setUsersOnSite(List.of(user));

        assertThat(service.isUserOnSite(user, site)).isTrue();
    }

    @Test
    void shouldNotFindUserOnSite() {
        User user = new User();
        PlaySite site = new Carousel();

        assertThat(service.isUserOnSite(user, site)).isFalse();
    }

    @Test
    void shouldFindUserAmongAllSites() {
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        PlaySite site1 = new Carousel();
        PlaySite site2 = new Carousel();
        site1.setUsersOnSite(List.of(user1));
        site2.setUsersInQueue(List.of(user2));

        given(repository.getAll()).willReturn(List.of(site1, site2));

        assertThat(service.isUserOnAnySite(user1)).isTrue();
        assertThat(service.isUserOnAnySite(user2)).isTrue();
        assertThat(service.isUserOnAnySite(user3)).isFalse();
    }

    @Test
    void shouldCheckForAvailablePlace() {
        PlaySite site = new Carousel();
        site.setMaxCapacity(1);
        assertThat(service.isPlaceAvailable(site)).isTrue();

        site.setUsersOnSite(List.of(new User()));
        assertThat(service.isPlaceAvailable(site)).isFalse();

        site.setUsersOnSite(List.of(new User(), new User()));
        assertThat(service.isPlaceAvailable(site)).isFalse();
    }
}