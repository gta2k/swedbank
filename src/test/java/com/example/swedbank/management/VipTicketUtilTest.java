package com.example.swedbank.management;

import com.example.swedbank.user.TicketType;
import com.example.swedbank.user.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class VipTicketUtilTest {

    @Test
    void shouldNotMutateIfEmpty() {
        List<User> queue = new ArrayList<>();

        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.isEmpty()).isTrue();
    }

    @Test
    void shouldNotMutateIfOne() {
        List<User> queue = List.of(new User());

        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.size()).isEqualTo(1);
    }

    @Test
    void shouldNotMutateIfVipRegular() {
        List<User> queue = new ArrayList<>();
        queue.add(User.builder().ticketType(TicketType.VIP).build());
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());

        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.get(0).getTicketType()).isEqualTo(TicketType.VIP);
        assertThat(queue.get(1).getTicketType()).isEqualTo(TicketType.REGULAR);
    }

    @Test
    void shouldMutateIfRegularVIP() {
        List<User> queue = new ArrayList<>();
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.VIP).build());

        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.get(0).getTicketType()).isEqualTo(TicketType.VIP);
        assertThat(queue.get(1).getTicketType()).isEqualTo(TicketType.REGULAR);
    }

    @Test
    void shouldBeVRRRVRR() {
        List<User> queue = new ArrayList<>();
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.REGULAR).build());
        queue.add(User.builder().ticketType(TicketType.VIP).build());

        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.get(0).getTicketType()).isEqualTo(TicketType.VIP);
        assertThat(queue.get(1).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(2).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(3).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(4).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(5).getTicketType()).isEqualTo(TicketType.REGULAR);

        queue.add(User.builder().ticketType(TicketType.VIP).build());
        VipTicketUtil.mutateQueue(queue);
        assertThat(queue.get(0).getTicketType()).isEqualTo(TicketType.VIP);
        assertThat(queue.get(1).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(2).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(3).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(4).getTicketType()).isEqualTo(TicketType.VIP);
        assertThat(queue.get(5).getTicketType()).isEqualTo(TicketType.REGULAR);
        assertThat(queue.get(6).getTicketType()).isEqualTo(TicketType.REGULAR);
    }

}