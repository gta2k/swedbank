package com.example.swedbank.management;

import com.example.swedbank.exception.UserAlreadyOnSite;
import com.example.swedbank.exception.UserIsNotOnPlaySite;
import com.example.swedbank.exception.UserWillNotWait;
import com.example.swedbank.playsite.PlaySite;
import com.example.swedbank.playsite.PlaySiteRepository;
import com.example.swedbank.playsite.types.Carousel;
import com.example.swedbank.user.User;
import com.example.swedbank.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class ManagementServiceTest {

    @MockBean private UserRepository userRepository;
    @MockBean private PlaySiteRepository playSiteRepository;
    @Autowired private ManagementService managementService;

    @Test
    void shouldAddBothOnSite() {
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user2.setId(2);
        user2.setWillWait(true);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setMaxCapacity(1);

        given(userRepository.getById(1)).willReturn(user1);
        given(userRepository.getById(2)).willReturn(user2);
        given(playSiteRepository.getById(1)).willReturn(playSite);

        managementService.addUser(1, 1);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(1);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);

        managementService.addUser(2, 1);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(1);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(1);
    }

    @Test
    void shouldNotAddSecondsTime() {
        User user1 = new User();
        user1.setId(1);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setUsersOnSite(List.of(user1));

        given(userRepository.getById(1)).willReturn(user1);
        given(playSiteRepository.getAll()).willReturn(List.of(playSite));
        given(playSiteRepository.getById(1)).willReturn(playSite);

        assertThatThrownBy(() -> managementService.addUser(1, 1)).isInstanceOf(UserAlreadyOnSite.class);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(1);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);
    }

    @Test
    void shouldNotAddToQueue() {
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user2.setId(2);
        user2.setWillWait(false);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setMaxCapacity(1);
        playSite.setUsersOnSite(List.of(user1));

        given(userRepository.getById(1)).willReturn(user1);
        given(userRepository.getById(2)).willReturn(user2);
        given(playSiteRepository.getById(1)).willReturn(playSite);

        assertThatThrownBy(() -> managementService.addUser(2, 1)).isInstanceOf(UserWillNotWait.class);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(1);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);
    }

    @Test
    void shouldRemoveUserFromSite() {
        User user1 = new User();
        user1.setId(1);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setMaxCapacity(1);

        given(userRepository.getById(1)).willReturn(user1);
        given(playSiteRepository.getById(1)).willReturn(playSite);
        managementService.addUser(1, 1);

        managementService.removeUser(1, 1);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(0);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);
    }

    @Test
    void shouldRemoveUserFromQueue() {
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user2.setId(2);
        user2.setWillWait(true);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setMaxCapacity(1);

        given(userRepository.getById(1)).willReturn(user1);
        given(userRepository.getById(2)).willReturn(user2);
        given(playSiteRepository.getById(1)).willReturn(playSite);
        managementService.addUser(1, 1);
        managementService.addUser(2, 1);

        managementService.removeUser(2, 1);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(1);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);
    }

    @Test
    void shouldMoveUserFromQueueToSite() {
        User user1 = new User();
        user1.setId(1);
        User user2 = new User();
        user2.setId(2);
        user2.setWillWait(true);

        PlaySite playSite = new Carousel();
        playSite.setId(1);
        playSite.setMaxCapacity(1);

        given(userRepository.getById(1)).willReturn(user1);
        given(userRepository.getById(2)).willReturn(user2);
        given(playSiteRepository.getById(1)).willReturn(playSite);
        managementService.addUser(1, 1);
        managementService.addUser(2, 1);

        managementService.removeUser(1, 1);
        assertThat(playSite.getUsersOnSite().get(0)).isEqualTo(user2);
        assertThat(playSite.getUsersInQueue().isEmpty()).isTrue();
    }

    @Test
    void shouldThrowExceptionIfUserIsNotOnSite() {
        User user1 = new User();
        user1.setId(1);
        PlaySite playSite = new Carousel();
        playSite.setId(1);

        given(userRepository.getById(1)).willReturn(user1);
        given(playSiteRepository.getById(1)).willReturn(playSite);

        assertThatThrownBy(() -> managementService.removeUser(1, 1)).isInstanceOf(UserIsNotOnPlaySite.class);
        assertThat(playSite.getUsersOnSite().size()).isEqualTo(0);
        assertThat(playSite.getUsersInQueue().size()).isEqualTo(0);
    }
}