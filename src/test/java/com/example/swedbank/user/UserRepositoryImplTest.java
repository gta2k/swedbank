package com.example.swedbank.user;

import com.example.swedbank.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@SpringBootTest
class UserRepositoryImplTest {

    @Autowired
    private UserRepository repository;

    @Test
    void shouldFindJustSavedUser() {
        User user = repository.add(new User());
        assertThat(repository.getById(user.getId())).isEqualTo(user);
    }

    @Test
    void shouldThrowException() {
        assertThatThrownBy(() -> repository.getById(0))
                .isInstanceOf(UserNotFoundException.class);
    }

    @Test
    void shouldHaveIncreasedUserId() {
        int lastUserId = repository.getAll().size();

        User newUser = repository.add(new User());

        assertThat(newUser.getId()).isEqualTo(lastUserId + 1);
    }
}