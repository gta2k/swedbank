package com.example.swedbank.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private Integer id;
    private Integer age;
    private String name;
    private String ticketNumber;
    private boolean willWait;
    private String ticketType;
}
