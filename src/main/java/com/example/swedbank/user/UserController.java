package com.example.swedbank.user;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("user")
public class UserController {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @PostMapping
    public UserDto add(@RequestBody UserDto dto) {
        User user = userRepository.add(userMapper.ToEntity(dto));
        return userMapper.toDto(user);
    }

    @GetMapping
    public List<UserDto> list() {
        return userRepository.getAll()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }
}

