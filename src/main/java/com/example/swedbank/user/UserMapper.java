package com.example.swedbank.user;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User ToEntity(UserDto userDto);
    UserDto toDto(User user);
}
