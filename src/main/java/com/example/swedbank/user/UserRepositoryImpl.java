package com.example.swedbank.user;

import com.example.swedbank.exception.UserNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final AtomicInteger idCounter = new AtomicInteger();
    private final List<User> userList = new ArrayList<>();

    @Override
    public List<User> getAll() {
        return userList;
    }

    @Override
    public User getById(int id) {
        return userList.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User add(User user) {
        user.setId(idCounter.incrementAndGet());
        userList.add(user);
        return user;
    }

    @Override
    public void remove(User user) {

    }
}
