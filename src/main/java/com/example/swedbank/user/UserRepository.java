package com.example.swedbank.user;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    User add(User user);

    void remove(User user);
}
