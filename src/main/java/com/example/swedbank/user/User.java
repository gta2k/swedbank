package com.example.swedbank.user;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private Integer age;
    private String name;
    private String ticketNumber;
    private boolean willWait;
    private TicketType ticketType;
}
