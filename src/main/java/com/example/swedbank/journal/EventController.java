package com.example.swedbank.journal;

import com.example.swedbank.playsite.PlaySiteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("events")
public class EventController {

    private final static DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final EventRepository repository;
    private final PlaySiteService playSiteService;

    @GetMapping("/{userId}")
    public List<EventDto> userActivity(@PathVariable Integer userId) {
        return repository.getLastEventsByUserId(userId)
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private EventDto toDto(Event event) {
        return EventDto.builder()
                .startedAt(event.getStartedAt().format(TIME_FORMAT))
                .finishedAt(event.getFinishedAt() != null ? event.getFinishedAt().format(TIME_FORMAT) : "not finished")
                .duration(getDuration(event))
                .eventType(event.getEventType().toString())
                .playSite(playSiteService.getById(event.getPlaySiteId()).getName())
                .build();
    }

    private String getDuration(Event event) {
        LocalDateTime finished = event.getFinishedAt() == null
                ? LocalDateTime.now()
                : event.getFinishedAt();

        Duration diff = Duration.between(event.getStartedAt(), finished);

        return String.format("%d:%02d:%02d",
                diff.toHours(),
                diff.toMinutesPart(),
                diff.toSecondsPart());
    }
}
