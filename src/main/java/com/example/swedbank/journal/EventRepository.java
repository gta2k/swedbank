package com.example.swedbank.journal;

import java.util.List;
import java.util.stream.Stream;

public interface EventRepository {
    Stream<Event> getAll();

    List<Event> getLastEventsByUserId(Integer userId);

    void startEvent(Integer userId, Integer playSiteId, EventType eventType);

    void finishEvent(Integer userId);
}
