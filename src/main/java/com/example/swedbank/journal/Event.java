package com.example.swedbank.journal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class Event implements Comparable<Event> {
    private Integer userId;
    private Integer playSiteId;
    private EventType eventType;
    private LocalDateTime startedAt;
    private LocalDateTime finishedAt;

    @Override
    public int compareTo(Event o) {
        return startedAt.compareTo(o.startedAt);
    }

}
