package com.example.swedbank.journal;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class EventRepositoryImpl implements EventRepository {
    private final List<Event> eventList = new ArrayList<>();
    private static final int MAX_EVENTS = 20;

    @Override
    public Stream<Event> getAll() {
        return eventList.stream().sorted(Comparator.reverseOrder());
    }

    @Override
    public List<Event> getLastEventsByUserId(Integer userId) {
        return getAll()
                .limit(MAX_EVENTS)
                .filter(event -> Objects.equals(event.getUserId(), userId))
                .collect(Collectors.toList());
    }

    @Override
    public void startEvent(Integer userId, Integer playSiteId, EventType eventType) {
        Event event = Event.builder()
                .userId(userId)
                .playSiteId(playSiteId)
                .eventType(eventType)
                .startedAt(LocalDateTime.now())
                .build();

        eventList.add(event);
    }

    @Override
    public void finishEvent(Integer userId) {
        getAll()
                .filter(event -> Objects.equals(event.getUserId(), userId))
                .findFirst()
                .ifPresent(event -> event.setFinishedAt(LocalDateTime.now()));
    }
}
