package com.example.swedbank.journal;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class EventDto {
    private String startedAt;
    private String finishedAt;
    private String duration;
    private String eventType;
    private String playSite;
}
