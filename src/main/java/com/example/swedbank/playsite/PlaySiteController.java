package com.example.swedbank.playsite;

import com.example.swedbank.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("playsite")
public class PlaySiteController {

    private final PlaySiteService playSiteService;

    @GetMapping
    public List<PlaySiteDto> list() {
        return playSiteService.getAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private PlaySiteDto toDto(PlaySite site) {
        return PlaySiteDto.builder()
                .id(site.getId())
                .name(site.getName())
                .onSiteUsers(site.getUsersOnSite().stream().map(User::getId).collect(Collectors.toList()))
                .inQueueUsers(site.getUsersInQueue().stream().map(User::getId).collect(Collectors.toList()))
                .maxCapacity(site.getMaxCapacity())
                .utilization(site.utilization())
                .build();
    }
}
