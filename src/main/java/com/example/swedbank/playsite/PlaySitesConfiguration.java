package com.example.swedbank.playsite;

import com.example.swedbank.playsite.types.*;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isBlank;

@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class PlaySitesConfiguration {

    private final PlaySiteService playSiteService;
    private final List<SiteDetails> playSites;

    @PostConstruct
    private void postProcessor() {

        for (SiteDetails details : playSites) {

            PlaySite site = switch (details.type) {
                case "BallPit" -> new BallPit();
                case "Carousel" -> new Carousel();
                case "Slide" -> new Slide();
                case "DoubleSwings" -> new DoubleSwings();
                case null, default -> throw new IllegalArgumentException("Unsupported play site");
            };

            String name = isBlank(details.name)
                    ? site.getClass().getSimpleName()
                    : details.name;

            site.setName(name);
            site.setMaxCapacity(details.capacity);

            playSiteService.add(site);
        }

    }

    @Data
    private static class SiteDetails {
        private String type;
        private String name;
        private Integer capacity;
    }

}
