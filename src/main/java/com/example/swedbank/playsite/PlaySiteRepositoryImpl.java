package com.example.swedbank.playsite;

import com.example.swedbank.exception.PlaySiteNotFound;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class PlaySiteRepositoryImpl implements PlaySiteRepository {
    private final AtomicInteger idCounter = new AtomicInteger();
    private final List<PlaySite> playSiteList = new ArrayList<>();

    @Override
    public List<PlaySite> getAll() {
        return playSiteList;
    }

    @Override
    public PlaySite getById(int id) {
        return playSiteList.stream()
                .filter(playSite -> playSite.getId() == id)
                .findFirst()
                .orElseThrow(PlaySiteNotFound::new);
    }

    @Override
    public PlaySite add(PlaySite playSite) {
        playSite.setId(idCounter.incrementAndGet());
        playSiteList.add(playSite);
        return playSite;
    }

    @Override
    public void remove(PlaySite playSite) {
        playSiteList.remove(playSite);
    }
}
