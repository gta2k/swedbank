package com.example.swedbank.playsite;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class PlaySiteDto {
    private Integer id;
    private String name;
    private List<Integer> onSiteUsers;
    private List<Integer> inQueueUsers;
    private Integer maxCapacity;
    private Integer utilization;
}
