package com.example.swedbank.playsite;

import com.example.swedbank.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaySiteService {
    private final PlaySiteRepository repository;

    public List<PlaySite> getAll() {
        return repository.getAll();
    }

    public PlaySite getById(Integer siteId) {
        return repository.getById(siteId);
    }

    public PlaySite add(PlaySite site) {
        return repository.add(site);
    }

    public List<User> addUserToSite(User user, PlaySite site) {
        site.getUsersOnSite().add(user);
        return site.getUsersOnSite();
    }

    public List<User> addUserToQueue(User user, PlaySite site) {
        site.getUsersInQueue().add(user);
        return site.getUsersInQueue();
    }

    public void removeUser(User user, PlaySite site) {
        site.getUsersOnSite().remove(user);
        site.getUsersInQueue().remove(user);
    }

    public boolean isUserOnSite(User user, PlaySite site) {
        return site.getUsersOnSite().contains(user) || site.getUsersInQueue().contains(user);
    }

    public boolean isUserOnAnySite(User user) {
        return repository.getAll()
                .stream()
                .anyMatch(site -> isUserOnSite(user, site));
    }

    public boolean isPlaceAvailable(PlaySite site) {
        return site.getUsersOnSite().size() < site.getMaxCapacity();
    }

}
