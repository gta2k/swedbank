package com.example.swedbank.playsite;

import com.example.swedbank.user.User;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public abstract class PlaySite {

    private Integer id;
    private String name;
    protected Integer maxCapacity;
    protected List<User> usersOnSite = new ArrayList<>();
    protected List<User> usersInQueue = new ArrayList<>();

    public abstract int utilization();
}

