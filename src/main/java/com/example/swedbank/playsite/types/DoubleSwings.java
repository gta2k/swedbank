package com.example.swedbank.playsite.types;

import com.example.swedbank.playsite.PlaySite;

public class DoubleSwings extends PlaySite {
    @Override
    public int utilization() {
        return usersOnSite.size() == 2 ? 100 : 0;
    }
}
