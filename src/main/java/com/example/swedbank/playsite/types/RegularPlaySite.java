package com.example.swedbank.playsite.types;

import com.example.swedbank.playsite.PlaySite;

public abstract class RegularPlaySite extends PlaySite {
    @Override
    public int utilization() {
        return 100 * usersOnSite.size() / maxCapacity;
    }
}
