package com.example.swedbank.playsite;

import java.util.List;

public interface PlaySiteRepository {
    List<PlaySite> getAll();

    PlaySite getById(int id);

    PlaySite add(PlaySite playSite);

    void remove(PlaySite playSite);
}
