package com.example.swedbank.management;

import com.example.swedbank.exception.UserAlreadyOnSite;
import com.example.swedbank.exception.UserIsNotOnPlaySite;
import com.example.swedbank.exception.UserWillNotWait;
import com.example.swedbank.journal.EventRepository;
import com.example.swedbank.journal.EventType;
import com.example.swedbank.playsite.PlaySite;
import com.example.swedbank.playsite.PlaySiteService;
import com.example.swedbank.user.TicketType;
import com.example.swedbank.user.User;
import com.example.swedbank.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagementService {
    private final UserRepository userRepository;
    private final PlaySiteService playSiteService;
    private final EventRepository eventRepository;

    public void addUser(Integer userId, Integer siteId) {
        User user = userRepository.getById(userId);
        PlaySite site = playSiteService.getById(siteId);

        if (playSiteService.isUserOnAnySite(user)) {
            throw new UserAlreadyOnSite();
        }

        if (playSiteService.isPlaceAvailable(site)) {
            playSiteService.addUserToSite(user, site);
            eventRepository.startEvent(user.getId(), site.getId(), EventType.PLAYING);

        } else {
            enqueue(user, site);
        }
    }

    public void removeUser(Integer userId, Integer siteId) {
        User user = userRepository.getById(userId);
        PlaySite site = playSiteService.getById(siteId);

        if (!playSiteService.isUserOnSite(user, site)) {
            throw new UserIsNotOnPlaySite();
        }

        playSiteService.removeUser(user, site);
        eventRepository.finishEvent(user.getId());

        if (playSiteService.isPlaceAvailable(site) && !site.getUsersInQueue().isEmpty()) {
            User nextUser = site.getUsersInQueue().remove(0);
            eventRepository.finishEvent(nextUser.getId());

            playSiteService.addUserToSite(nextUser, site);
            eventRepository.startEvent(nextUser.getId(), site.getId(), EventType.PLAYING);
        }
    }

    private void enqueue(User user, PlaySite playSite) {
        if (!user.isWillWait()) {
            throw new UserWillNotWait();
        }

        List<User> queue = playSiteService.addUserToQueue(user, playSite);
        eventRepository.startEvent(user.getId(), playSite.getId(), EventType.IN_QUEUE);

        if (user.getTicketType() == TicketType.VIP) {
            VipTicketUtil.mutateQueue(queue);
        }

    }

}
