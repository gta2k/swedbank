package com.example.swedbank.management;

import com.example.swedbank.user.TicketType;
import com.example.swedbank.user.User;

import java.util.List;

public class VipTicketUtil {
    private static final int DISTANCE_BETWEEN_VIP = 3;

    public static void mutateQueue(List<User> queue) {
        if (queue.size() < 2) {
            return;
        }

        if (queue.get(0).getTicketType() == TicketType.REGULAR) {
            User vipUser = queue.remove(queue.size() - 1);
            queue.add(0, vipUser);
            return;
        }

        int regulars = 0;
        for (int i = 0; i < queue.size(); i++) {
            if (queue.get(i).getTicketType() == TicketType.REGULAR) {
                if (regulars == DISTANCE_BETWEEN_VIP) {
                    User vipUser = queue.remove(queue.size() - 1);
                    queue.add(i, vipUser);
                    return;
                }
                regulars++;

            } else {
                regulars = 0;
            }
        }
    }
}
