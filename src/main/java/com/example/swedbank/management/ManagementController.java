package com.example.swedbank.management;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("management")
public class ManagementController {

    private final ManagementService managementService;

    @PostMapping
    public void addUserToPlaySite(@RequestBody RequestDto request) {
        managementService.addUser(request.userId, request.siteId);
    }

    @DeleteMapping
    public void removeUserFromSite(@RequestBody RequestDto request) {
        managementService.removeUser(request.userId, request.siteId);
    }

    @Data
    static class RequestDto {
        private Integer userId;
        private Integer siteId;
    }

}
