package com.example.swedbank.snapshot;

import com.example.swedbank.playsite.PlaySite;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SnapshotService {

    private final SnapshotRepository snapshotRepository;

    public Snapshot takeSnapshot(PlaySite playSite) {
        Snapshot snapshot = Snapshot.builder()
                .dateTime(LocalDateTime.now().withSecond(0).withNano(0))
                .siteId(playSite.getId())
                .utilization(playSite.utilization())
                .build();

        snapshotRepository.addSnapshot(snapshot);
        return snapshot;
    }

    public SnapshotDto getSnapshots(PlaySite playSite) {
        Map<LocalDateTime, Integer> utilization = snapshotRepository.getUtilizationBySiteId(playSite.getId());

        return SnapshotDto.builder()
                .siteId(playSite.getId())
                .siteName(playSite.getName())
                .utilization(utilization)
                .build();
    }

}
