package com.example.swedbank.snapshot;

import java.time.LocalDateTime;
import java.util.Map;

public interface SnapshotRepository {
    void addSnapshot(Snapshot snapshot);

    Map<LocalDateTime, Integer> getUtilizationBySiteId(int id);
}
