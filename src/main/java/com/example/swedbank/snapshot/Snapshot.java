package com.example.swedbank.snapshot;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class Snapshot implements Comparable<Snapshot> {
    private final LocalDateTime dateTime;
    private final Integer siteId;
    private final Integer utilization;

    @Override
    public int compareTo(Snapshot o) {
        return dateTime.compareTo(o.dateTime);
    }
}
