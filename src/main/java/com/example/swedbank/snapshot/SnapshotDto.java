package com.example.swedbank.snapshot;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
class SnapshotDto {
    private Integer siteId;
    private String siteName;
    private Map<LocalDateTime, Integer> utilization;
}
