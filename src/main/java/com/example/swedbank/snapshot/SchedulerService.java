package com.example.swedbank.snapshot;

import com.example.swedbank.playsite.PlaySiteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@EnableScheduling
@RequiredArgsConstructor
public class SchedulerService {

    private final SnapshotService snapshotService;
    private final PlaySiteService playSiteService;

    @Scheduled(timeUnit = TimeUnit.MINUTES, fixedRateString = "${application.snapshot-interval}")
    public void takeSnapshot() {
        log.debug("Taking an utilization snapshot");
        playSiteService.getAll().forEach(snapshotService::takeSnapshot);
    }

}
