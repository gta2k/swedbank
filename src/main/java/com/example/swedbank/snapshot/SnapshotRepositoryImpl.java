package com.example.swedbank.snapshot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

import static java.util.stream.Collectors.toMap;

@Service
public class SnapshotRepositoryImpl implements SnapshotRepository {
    private final List<Snapshot> snapshotList = new ArrayList<>();

    @Value("${application.report-size}")
    private Integer NUMBER_OF_SNAPSHOTS;

    @Override
    public void addSnapshot(Snapshot snapshot) {
        snapshotList.add(snapshot);
    }

    @Override
    public Map<LocalDateTime, Integer> getUtilizationBySiteId(int id) {
        return snapshotList.stream()
                .filter(snapshot -> snapshot.getSiteId() == id)
                .sorted(Comparator.reverseOrder())
                .limit(NUMBER_OF_SNAPSHOTS)
                .collect(toMap(Snapshot::getDateTime, Snapshot::getUtilization, (o1, o2) -> o1, TreeMap::new));
    }
}
