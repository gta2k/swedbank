package com.example.swedbank.snapshot;

import com.example.swedbank.playsite.PlaySiteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("snapshot")
public class SnapshotController {

    private final SnapshotService snapshotService;
    private final PlaySiteService playSiteService;

    @GetMapping
    public List<SnapshotDto> get() {
        return playSiteService.getAll()
                .stream()
                .map(snapshotService::getSnapshots)
                .collect(Collectors.toList());
    }
}
