package com.example.swedbank;

import com.example.swedbank.user.TicketType;
import com.example.swedbank.user.User;
import com.example.swedbank.user.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SwedbankApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwedbankApplication.class, args);
    }

    @Bean
    CommandLineRunner initUsers(UserRepository userRepository) {
        return args -> {
            userRepository.add(createUser("Leanne Graham", 11, "A123", TicketType.REGULAR, true));
            userRepository.add(createUser("Ervin Howell", 8, "A124", TicketType.REGULAR, true));
            userRepository.add(createUser("Clementine Bauch", 17, "B15", TicketType.VIP, true));
            userRepository.add(createUser("Patricia Lebsack", 14, "A125", TicketType.REGULAR, true));
            userRepository.add(createUser("Chelsey Dietrich", 31, "B52", TicketType.VIP, true));
            userRepository.add(createUser("Dennis Schulist", 12, "XYZ999", TicketType.REGULAR, true));
            userRepository.add(createUser("Kurtis Weissnat", 11, "N600", TicketType.REGULAR, false));
        };
    }

    private User createUser(String name, Integer age, String ticketNumber, TicketType ticketType, boolean willWait) {
        User user = new User();
        user.setName(name);
        user.setAge(age);
        user.setTicketNumber(ticketNumber);
        user.setTicketType(ticketType);
        user.setWillWait(willWait);
        return user;
    }
}
