package com.example.swedbank.exception;

public class UserIsNotOnPlaySite extends RuntimeException {
    public UserIsNotOnPlaySite() {
        super("This user is not assigned to play site");
    }
}
