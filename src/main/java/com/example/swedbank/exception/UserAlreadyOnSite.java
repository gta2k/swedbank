package com.example.swedbank.exception;

public class UserAlreadyOnSite extends RuntimeException {
    public UserAlreadyOnSite() {
        super("This user is already playing");
    }
}
