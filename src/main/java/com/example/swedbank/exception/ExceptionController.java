package com.example.swedbank.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler({
            UserNotFoundException.class,
            PlaySiteNotFound.class
    })
    public ResponseEntity<String> handleNotFoundException(Exception ex) {
        return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
    }

    @ExceptionHandler({
            UserWillNotWait.class,
            UserAlreadyOnSite.class,
            UserIsNotOnPlaySite.class
    })
    public ResponseEntity<ErrorContainer> handleBadRequestException(Exception ex) {
        return ResponseEntity.status(BAD_REQUEST).body(new ErrorContainer(ex.getMessage()));
    }

}
