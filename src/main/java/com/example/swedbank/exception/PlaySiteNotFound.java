package com.example.swedbank.exception;

public class PlaySiteNotFound extends RuntimeException {
    public PlaySiteNotFound() {
        super("Play site not found");
    }
}
