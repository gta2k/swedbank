package com.example.swedbank.exception;

public class UserWillNotWait extends RuntimeException {
    public UserWillNotWait() {
        super("User will not wait");
    }
}
