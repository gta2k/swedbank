# Test task for Swedbank

### Description

The application is about managing play sites and kids on them, monitoring site's utilization and kid's activity history.  
Read full description [here](SoftwareEngineerJavaHomework.pdf).

This is a Gradle project. Created in IntelliJ IDEA.    
Made of the latest Java 17 including preview features. You will have to explicitly enable them in IntelliJ project settings.  

### Configuration

Play sites are configured in application.yml.  
Each of them can have unique name and number of seats (capacity).  
Any new attraction can be easily extended from abstract PlaySite class.

A kid can be assigned to a play site either playing or standing in queue (if full). All his activity is logged and available at any time.  
Play site utilization snapshots are done regularly with interval set in application.yml. Data is presented as graph and available at any time.

## Demo

For demo purposes I deployed the application to Google Cloud Platform. Check it [here](http://swedbank.demo.belous.dev/)