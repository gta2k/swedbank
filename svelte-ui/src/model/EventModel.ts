export type EventModel = {
    startedAt: string
    finishedAt: string
    duration: string
    eventType: string
    playSite: string
}