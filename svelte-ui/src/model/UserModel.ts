export enum TicketType {
    REGULAR = 'REGULAR',
    VIP = 'VIP'
}

export type UserModel = {
    id: number
    name: string
    age: number
    ticketNumber: string
    ticketType: TicketType
    willWait: boolean
}