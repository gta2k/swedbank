
export type PlaysiteModel = {
    id: number
    name: string
    onSiteUsers: number[]
    inQueueUsers: number[]
    maxCapacity: number
    utilization: number
}