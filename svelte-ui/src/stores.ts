import {writable} from 'svelte/store'
import type {UserModel} from "./model/UserModel";
import type {PlaysiteModel} from "./model/PlaysiteModel";

export const UserStore = writable(Array<UserModel>())
export const PlaysiteStore = writable(Array<PlaysiteModel>())