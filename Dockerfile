FROM node AS node
WORKDIR /svelte-ui
COPY svelte-ui ./
RUN npm install
RUN npm run build

FROM bellsoft/liberica-openjdk-alpine AS build
WORKDIR /build
COPY . ./
COPY --from=node /svelte-ui/build/ ./src/main/resources/public/
RUN sh gradlew build

FROM bellsoft/liberica-openjdk-alpine
EXPOSE 8080
COPY --from=build /build/build/libs/*SNAPSHOT.jar app.jar
CMD ["java","--enable-preview","-jar","app.jar"]
